
## Getting Started

First clone this repo to get the same developement environment:
```
git clone https://code.umd.edu/robotics-at-maryland/website.git
```
Then to install the project dependencies run:
```
yarn install
```

Then to run the development server:
```
yarn dev
```
Then, open [http://localhost:3000](http://localhost:3000) with your browser to see the website.

### Getting started using Docker

Clone the repo.

- Build the container: `make container`
- Run the container: `make server`
    - Then, open [http://localhost:3000](http://localhost:3000) with your browser to see the website.
- To stop the container: `docker stop ram-website`

## Info

The project using the Page Router structure. Read more in the [Next.js Docs](https://nextjs.org/docs/pages/building-your-application/routing/pages-and-layouts).


### Component Library

Using Material UI component library check out the [Docs](https://mui.com/material-ui/getting-started/overview/) for a list of components and examples, many you can just use as is.

To customize components Material UI uses the custom sytling engine Emotion to make it easier than raw CSS and port it into JS. This is done by passing an object into the sx prop of any Material UI component, with each property being a CSS property in camel case. Read more in the [docs](https://mui.com/system/getting-started/the-sx-prop/). 

Example:
```
export default function Example() {
  return (
    <ThemeProvider theme={theme}>
      <Box
        sx={{
          bgcolor: 'background.paper',
          boxShadow: 1,
          borderRadius: 2,
          p: 2,
          minWidth: 300,
        }}
      >
        <Box sx={{ color: 'text.secondary' }}>Sessions</Box>
        <Box sx={{ color: 'text.primary', fontSize: 34, fontWeight: 'medium' }}>
          98.3 K
        </Box>
        <Box
          sx={{
            color: 'success.dark',
            display: 'inline',
            fontWeight: 'bold',
            mx: 0.5,
            fontSize: 14,
          }}
        >
          +18.77%
        </Box>
        <Box sx={{ color: 'text.secondary', display: 'inline', fontSize: 14 }}>
          vs. last week
        </Box>
      </Box>
    </ThemeProvider>
  );
}
```

### Building

#### Build site

Running:

```
yarn build
```

Will create a static website build (HTML/CSS) in the out folder.

You can also use: `docker build`

#### Optimize media (images/videos)

Media should be stored in the `media/` directory. Next.js looks for it in the `public/` directory, but we have a script to copy the optimized files to there.

Optimize files: `./scripts/shrink`

NOTE: The shrink script is currently only aware of certain file extensions. If your image isn't being copied to the `public/` directory by the script, check the script source code and make sure it handles the file extension you're using. Adding support for a new file extension should be a simple matter of copying the shell expansion glob for an existing extension and changing only the extension.
#### Strip EXIF information from images

https://photo.stackexchange.com/a/69742

```
exiftool -all= -TagsFromFile @ -ColorSpaceTags FILES_OR_DIR
```

Check for warnings like "Image colors may be affected", and if you see them compare the images to see if it changed visually in a way you don't like. Convenient way to compare them is to open them both up, fullscreen/overlap the windows, then Alt-Tab / Alt-\` between the images.

#### Rotate an image with ImageMagick

```
convert -rotate 90 INPUT_FILE OUTPUT_FILE
```

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!


