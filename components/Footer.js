import * as React from 'react';
import { Container, Grid, Typography, Button, IconButton } from '@mui/material';
import {Twitter, GitHub, YouTube, Instagram} from '@mui/icons-material';
import Image from '@/components/Image'
const Logo = {
    src: '/Logo2_Final.png',
    alt: 'Robotics At Maryland Logo'
}

export default function Footer() {
    return (
        <Container
            maxWidth={false}
            disableGutters
            sx={{
                p:5,
                backgroundColor:'common.black'
            }}
        >
            <Grid
                container
                spacing={{ xs: 2, md: 3 }}
                
                textAlign='left'
            >
                {/* logo */}
                <Grid item xs={2} md={1} minHeight={100} alignItems='flex-start'>
                    <Image src={Logo.src} alt={Logo.alt} height={100} sx={{backgroundSize:'contain'}} />
                </Grid>

                <Grid container item xs={10} md={8}>
                    <Grid item xs={12}> 
                        <Typography variant='h6' color='commmon.white'>Robotics at Maryland</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography paragraph color='commmon.white' variant='subtitle2'>Neutral Buoyancy Research Facility, 4436 Technology Drive, College Park, MD, 20742</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography paragraph color='commmon.white' variant='subtitle2'>3rd Floor Robotics Lab, E.A. Fernandez IDEA Factory, 4462 Stadium Drive, College Park, MD, 20742</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography paragraph color='commmon.white' variant='subtitle2' fontWeight={700}>Meetings 7 - 9 PM Wednesdays, 1 - 4 PM Sundays, at IDEA Factory</Typography>
                    </Grid>
                </Grid>
               
                <Grid container item xs={12} md={3} textAlign='center'>
                    <Grid item xs={12} sx={{py:1}}>
                        <Button variant='contained' href='mailto:team@ram.umd.edu' >Contact Us</Button>
                    </Grid>
                    <Grid item xs={12} md={4} sx={{py:1}}>
                        <Typography variant='body1' color='commmon.white'>Our Platforms:</Typography>
                    </Grid>
                    <Grid item xs={3} md={2}>
                        <IconButton size="large" href="https://twitter.com/projectqubo" >
                            <GitHub/>
                        </IconButton>
                    </Grid>
                    <Grid item xs={3} md={2}>
                        <IconButton size="large" href="https://twitter.com/projectqubo">
                            <Twitter/>
                        </IconButton>
                    </Grid>
                    <Grid item xs={3} md={2}>
                        <IconButton size="large" href="https://twitter.com/projectqubo">
                            <Instagram/>
                        </IconButton>
                    </Grid>
                    <Grid item xs={3} md={2}>
                        <IconButton size="large" href="https://twitter.com/projectqubo">
                            <YouTube/>
                        </IconButton>
                    </Grid>
                </Grid>

            </Grid>
        </Container>
    )
}