import Head from "next/head";

export default function SEO(props){

    props = {
        ...{keywords:"University of Maryland, Robotics, Engineering, Software, Club"}
    };

    return(
        <Head>
            <title>Robotics at Maryland</title>
            <meta name="description" content={props.description || "University of Maryland Student Robotics Club"} key="desc" />
            {/* social */}
            <meta property="og:title" content={props.title} key="Robotics at Maryland"/>
            <meta
                property="og:description"
                content="University of Maryland premier student-run robotics club."
            />
            <meta
                property="og:image"
                content="/Logo2_Final.png"
            />
            <meta name="keywords" content={props.keywords} key="keywords"/>
        </Head>
    )


}