import React, { useState, useEffect} from 'react';

import { styled, alpha } from '@mui/material/styles';

import AppBar from '@mui/material/AppBar'; 
import Toolbar from '@mui/material/Toolbar'; 
import Button from '@mui/material/Button'; 
import Box from '@mui/material/Box'; 
import Typography from '@mui/material/Typography'; 
import InputBase from '@mui/material/InputBase';
import Menu from '@mui/material/Menu';

import SearchIcon from '@mui/icons-material/Search'; 
import IconButton from '@mui/material/Typography'; 
import MenuIcon from '@mui/icons-material/Menu';
import logo from '../public/Logo2_Final.png'; 
import MenuItem from '@mui/material/MenuItem';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';


import { createTheme, useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import Script from 'next/script';
import Head from 'next/head';

'use client';

const pages = {
  "Home": "/",
  "Robosub":"/robots/qubo", 
  "Testudog": "/robots/testudog",
  "About":"/about-us", 
  "Sponsors":"/sponsors",
  "Documentation":"https://code.umd.edu/robotics-at-maryland/ram/-/wikis/home"
};

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(1),
    width: 'auto',
  },
  [theme.breakpoints.between('0', '450')]: {
    width: '0ch',
    '&:focus': {
      width: '0ch',
    },
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  [theme.breakpoints.between('0', '450')]: {
    width: '0ch',
    '&:focus': {
      width: '0ch',
    },
  },
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
    [theme.breakpoints.between('450','sm')]: {
      width: '0ch',
      '&:focus': {
        width: '6ch',
      },
    },
    [theme.breakpoints.between('0', '450')]: {
      width: '0ch',
      '&:focus': {
        width: '0ch',
      },
    },
  },
}));

const SearchContainer = styled(Box)(({theme}) => ({
  [theme.breakpoints.between('0', '450')]: {
    display: "none"
  },
}))

const handleOpenNavMenu = (event) => {
  setAnchorElNav(event.currentTarget);
};

const handleCloseNavMenu = () => {
  setAnchorElNav(null);
};

const ButtonContainer_LargeDevices = styled(Box)(({theme}) => ({
  padding: theme.spacing(1),
  [theme.breakpoints.up("1150")]: {
    display: "flex"
  },
  [theme.breakpoints.between("0", "1150")]: {
    display: "none"
  }
}));

const ButtonContainer_SmallDevices = styled(Box)(({theme}) => ({
  [theme.breakpoints.up("1150")]: {
    display: "none"
  },
  [theme.breakpoints.between("0", "1150")]: {
    display: "flex"
  }
}))


function Custom_Menu(props){
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.between('0', '1150'));
  // const matches = useMediaQuery(theme.breakpoints.up('sm'));
  const pages = props.pages;
  const anchorElNav = props.anchorElNav;
  const setAnchorElNav = props.setAnchorElNav;
  if(matches){
      return(
        
        <Menu
          id="menu-appbar"
          anchorEl={anchorElNav}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          keepMounted
          transformOrigin={{
            vertical: 'top',
            horizontal: 'left',
          }}
          open={Boolean(anchorElNav)}
          onClose={() => setAnchorElNav(null)}
          sx={{
            display: 'block'
          }}
          disableAutoFocusItem
        >
          {Object.keys(pages).map((page) => (
                <MenuItem key={page} onClick={() => setAnchorElNav(null)}>
                  <Typography textAlign="center" color='#F9F2E6' component="a" href={pages[page]} sx={{textDecoration:'none'}}>{page}</Typography>
                </MenuItem>
          ))}
          
        </Menu>
      )
  } else {
    return (<></>)
  }
}

function SearchBox(props){
  const theme = useTheme();
  
  if(props.ifClient){
    return(
        <div className="gcse-search" key="search" style={{width:'1000vh'}} ></div>
    )
  }
}

// const StyledSearchBox = styled((props) => {(
//   props.ifClient?(<div className="gcse-search"></div>):(<></>)
// )})(({ theme }) => ({
//   [theme.breakpoints.up("450")]: {
//     display: "flex"
//   },
//   [theme.breakpoints.between("0", "450")]: {
//     display: "none"
//   }
// }));


const NavigationBar = () => {

  const [anchorElNav, setAnchorElNav] = React.useState(null);

  const [clientSideRendering, setClientSideRendering] = React.useState(false);

  useEffect(() => {
    // update some client side state to say it is now safe to render the client-side only component
    setClientSideRendering(true);

    console.log("Client Side Rendering intiated")
  }, []);

  return (
    <>
      
      <Script async src="https://cse.google.com/cse.js?cx=15876c2a6c0974b92" strategy='afterInteractive'/>
      <AppBar position = "sticky" color = "primary" sx = {{height: "100px", display: "flex", flexDirection: "column", justifyContent: "center"}}>
        <Toolbar color = "primary" sx = {{display: "flex", alignItems: "center", justifyContent: "center", flexGrow: 1}}>

          <Box sx = {{height: "60px", width: "auto", paddingRight: "15px"}}
            component="img"
            alt="The house from the offer."
            src= '/Logo2_Final.png'
          />

          <Box sx = {{display: {xs:"none", sm:"flex"}, flexDirection: "column"}}>
            <Typography fontSize={25} sx = {{ fontWeight: "bold", color: "#F9F2E6", lineHeight: '24px'}}>Robotics at</Typography>
            <Typography fontSize={25} sx = {{ fontWeight: "bold", color: "#F9F2E6", lineHeight: '24px'}}>Maryland</Typography>
          </Box>

          <ButtonContainer_LargeDevices sx = {{flexGrow: 1, flexDirection: "row", alignItems: "center", justifyContent: "center", gap: "20px"}}>
            {Object.keys(pages).map((page) => (
              <Button color = "secondary" key={page} href={pages[page]}><Typography sx = {{fontSize: "17px", fontWeight:500 }}>{page}</Typography></Button>
            ))}
          </ButtonContainer_LargeDevices>


          


          <ButtonContainer_SmallDevices sx = {{flexGrow: 1, justifyContent: 'flex-end'}}>
              <Box sx = {{display: "flex", flexDirection: "row"}} onClick={(event) => setAnchorElNav(event.currentTarget)}>
                <Typography sx = {{fontSize: "18px", paddingTop: "5px"}}>Menu</Typography>
                <IconButton 
                  sx = {{paddingLeft: "2px", paddingTop: "6px"}}
                  size="large"
                  aria-controls="menu-appbar"
                  aria-haspopup="true"
                  color="inherit"
                >
                  <ArrowDownwardIcon />
                  {/* <Menu/> */}
                </IconButton>
              </Box>

              <Box sx = {{marginLeft: "10px", marginRight: "10px", borderRight: "2px solid white"}}></Box>
              {/* <Menu
                id="menu-appbar"
                anchorEl={anchorElNav}
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'left',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'left',
                }}
                open={Boolean(anchorElNav)}
                onClose={() => setAnchorElNav(null)}
                sx={{
                  display: { xs: 'block', md: 'none' },
                }}
              >
                {Object.keys(pages).map((page) => (
                  <MenuItem key={page} onClick={() => setAnchorElNav(null)}>
                    <Typography textAlign="center">{page}</Typography>
                  </MenuItem>
                ))}
              </Menu> */}
              
              <Custom_Menu pages={pages} anchorElNav={anchorElNav} setAnchorElNav={setAnchorElNav} />
          </ButtonContainer_SmallDevices>
          {/* <Box sx={{color:'black'}}>
            {clientSideRendering?(<div className="gcse-search"></div>):console.log("not client")}
          </Box> */}
          <SearchBox ifClient={clientSideRendering} style={{alignSelf:"flex-end"}}/>

          
         
          
          {/* {clientSideRendering?(<div className='gcse-search'></div>):(<></>)} */}
{/*           
          <SearchContainer>
            <Search>
              <SearchIconWrapper>
                <SearchIcon />
              </SearchIconWrapper>
              <StyledInputBase
                placeholder="Search…"
                inputProps={{ 'aria-label': 'search' }}
              />
            </Search>
          </SearchContainer> */}
        </Toolbar>
      </AppBar>
      
    </>
  )
}

export default NavigationBar
