'use client';
import * as React from 'react';
import Box from '@mui/material/Box';
export default function Image(props){
    return (
        <Box
            sx={Object.assign({
                backgroundImage: `url(${props.src})`,
                backgroundSize: props.backgroundSize || 'cover',
                backgroundRepeat: props.backgroundRepeat || 'no-repeat',
                backgroundPosition: props.backgroundPosition || 'center center',
                height: props.height || 1,
                width: props.width || 1,
                maxHeight:props.maxHeight,
                maxWidth:props.maxWidth,
                title:props.alt
            }, props.sx)}
            
        /> 
    )
}