IMAGE = ram/website

.PHONY: container
container: Dockerfile
	docker build --tag=$(IMAGE) ./

.PHONY: serve
serve: container
	docker run --name ram-website --rm --net host --detach --interactive --tty --mount type=bind,source="$(shell pwd)",target=/website $(IMAGE) yarn dev

.PHONY: build
build:
	docker run --name ram-website --rm --net host --interactive --tty --mount type=bind,source="$(shell pwd)",target=/website $(IMAGE) yarn build
