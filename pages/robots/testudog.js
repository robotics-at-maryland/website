'use client';

import NavigationBar from '@/components/NavigationBar';
import * as React from 'react';
import { Typography } from '@mui/material'; 
import Box from '@mui/material/Box';


export default function Testudog(){
    return (      
        <>
            <Box backgroundColor = "#FFFFFF" sx = {{height: "calc(100vh - 60px)", display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center"}}>
                <Typography fontSize = "30px" color = "black" sx = {{textDecoration: "underline"}}>Work in Progress</Typography>
                <Typography color = "black">Testudog is currently a work in progress.</Typography>
                <Typography color = "black">Please check back later to see updates!</Typography>
                <Typography color = "black">If you&apos;re interested in working on Testudog, please join the #testudog channel on our Slack.</Typography>
            </Box>
        </>
    )
}
