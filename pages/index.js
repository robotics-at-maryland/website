'use client';

import { Container, Box, Typography, Button, Grid, TextField} from "@mui/material";
import Image from '@/components/Image';
import SEO from "@/components/SEO";
import { useTheme } from "@emotion/react";
import YouTube from "react-youtube";
const HeroShot = {
    src: '/FadedNBRF.png',
    alt: 'NBRF Pool'
};
const QuboShot = {
    src: '/FullSizeRender.jpg',
    alt: 'Qubo Robot'
};
const QuboOnTable = {
    src:'/Qubo2.jpg',
    alt:'Qubo On Table'
};
const NBRFWindow = {
    src:'/NBRFWindow.jpg',
    alt:'NBRF Window'
};
const TeamSelfie = {
    src:'/TeamSelfie.jpg',
    alt:'Team Selfie'
};
const CNC = {
    src:'/CNC.jpg',
    alt:'CNC Machine'
};
const TestudogAssembling = {
    src:'/TestudogAssembiling.jpg',
    alt:'Testudog being assembeled'
};
const IdeaStepsImage = {
    src:'/IdeaSteps.png',
    alt:'Idea Factory Steps'
};
const TestudogShot = {
    src:'/TestudogAssembeled.png',
    alt:'Testudog Robot'
}

const Printer1 = {
    src: '/3DPrinter.jpg',
    alt:'3D Printer'
}
const Printer2 = {
    src: '/3DPrinter2.jpg',
    alt:'3D Printer'
}

const BuildingInNBRF = {
    src: '/NBRFBuilding.jpg',
    alt: 'The team building at NBRF'
}
// home page
export default function Index() {

    const theme = useTheme();
    return (
        <>
            <SEO/>
            {/* splash screen */}
            <Box
                sx={{
                    backgroundImage: `url(${HeroShot.src})`,
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',
                    height:900,
                    width:1,
                    py:10,
                    title:HeroShot.alt,
                    display: 'flex',
                    justifyContent: 'center'
                }}
            >
                <Container maxWidth='lg' 
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'space-evenly'
                    }}
                >
                    {/* Eye catching headline */}
                    <Typography variant='h1' textAlign='center' color='commmon.white' sx={{ fontWeight: 800 }}>Pioneering the future of autonmous systems.</Typography>
                    
                    {/* Action Button */}
                    {/* To Do separate into respective user type actions ie. sponsors new members */}
                    <Button
                        
                        variant='contained'
                        
                        
                        color='primary'

                        href='#calltoaction'
                        sx={{
                            width:250,
                            height:50,
                            alignSelf:'center',
                            textAlign:'center',
                            p:2

                        }}

                    >
                        <Typography variant='h5' fontWeight='800' color='commmon.white'>Get Involved</Typography>
                    </Button>
                    <Box>
                        <Typography variant='h3' textAlign='center' color='commmon.white'>University of Maryland</Typography>
                        <Typography variant='h4' textAlign='center' color='commmon.white'>Premier Student Robotics Club</Typography>
                    </Box>
                </Container>
                
            </Box>


            {/* Mission statement */}
            <Container
                maxWidth='xl'
                sx={{
                    
                    my:10
                }}
            >
                <Typography
                    variant='h3'
                    textAlign='center'
                    color='grey.200'
                    sx={{
                        fontWeight: 400
                    }}
                >
                    We are a diverse multi-disciplinary team of undergraduate students collaborating on solving real-world challenges and gaining hands-on experience in the process.
                </Typography>
            </Container>

            {/* Second info Module */}
            {/* Club Details */}
            <Container
                maxWidth='xl'
                sx={{
                    my:12
                }}
                
            >
                <Grid
                    container
                    spacing={3}
                    textAlign='center'
                    sx={{
                        py: 4
                    }}
                >
                    <Grid container item xs={12} md={6} spacing={3} minHeight={{xs:1200, md:900}}>
                        <Grid item xs={12}>
                            <Image src={TeamSelfie.src} alt={TeamSelfie.alt}/>
                            
                        </Grid>
                        <Grid item xs={12} >
                            <Image src={QuboOnTable.src} alt={QuboOnTable.alt} />
                        </Grid>
                    </Grid>
                    <Grid item xs={12} md={6} minHeight={{xs:600, md:900}}>
                        <Image src={NBRFWindow.src} alt={NBRFWindow.alt} />
                    </Grid>

                    <Grid item xs={6} md={3} minHeight={500}>
                            <Image src={CNC.src} alt={CNC.alt}/>
                        </Grid>
                    <Grid container item xs={6} md={3} spacing={3} minHeight={500}>
                        
                        <Grid item xs={12}>
                            <Image src={Printer1.src} alt={Printer1.alt}/>
                        </Grid>
                        <Grid item xs={12}>
                            <Image src={Printer2.src} alt={Printer2.alt}/>
                        </Grid>
                        
                    </Grid>

                    <Grid item xs={12} md={6} minHeight={500} >
                        <Image src={TestudogAssembling.src} alt={TestudogAssembling.alt} />
                    </Grid>

                    

                </Grid>
                <YouTube 
                    videoId="V3_ZrQ5SG2M" 
                    opts={{
                        height: "600",
                        width: "100%"
                    }}
                />
            </Container>

            <Container maxWidth='xl'>
                
            </Container>

            {/* Robots section */}
            <Box
                sx={{
                    backgroundColor:'primary.main',
                    width:1,
                    heigth:1,
                    mt:15
                }}
            >

            
                <Container
                    maxWidth='xl'
                    sx={{
                        backgroundColor:'primary.main',
                        py:15
                    }}
                    
                >
                    <Grid
                        container
                        spacing={{xs:10, md:3}}
                        rowSpacing={{xs:10, md:30}}
                        textAlign='center'
                    >
                        <Grid item xs={12} md={6} minHeight={{xs:600, md:800}}>
                            <Image src={QuboShot.src} alt={QuboShot.alt}/>
                        </Grid>
                        <Grid item sm={12} md={6} spacing={5} alignItems='center'>
                            <Container maxWidth='sm' sx={{width:1, height:1, display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
                                <Typography
                                    variant='h1'
                                    textAlign='center'
                                    color='commmon.white'
                                    fontWeight={800}
                                >
                                    Qubo
                                </Typography>

                                <Typography
                                    variant='h5'
                                    textAlign='left'
                                    color='commmon.white'
                                    sx={{my:3, py:3}}
                                >
                                    {/* Dive into Qubo, our second generation competitive autonomous underwater vehicle (AUV) pushing the boundaries of maritime robotics. Our club was founded around competitng in the <span style={{color: theme.palette.ternary.main, fontWeight:800}}>RoboSub</span> competition and it is our largest and longest project to-date. */}
        Dive into Qubo, our entrant for the <span style={{color: theme.palette.ternary.main, fontWeight:800}}>2023 RoboSub competition</span>. It is our second generation autonomous underwater vehicle (AUV) pushing the boundaries of maritime robotics. Robotics at Maryland&apos;s largest and longest project to-date.
                                </Typography>
                                <Button size='large' variant='contained' color='ternary' href='/robots/qubo' >
                                    <Typography variant='h5' color='common.black'>Learn More</Typography>
                                </Button>
                            </Container>
                        </Grid>
                        
                        <Grid item xs={12} md={6} alignItems='center'>
                            <Container maxWidth='sm' sx={{width:1, height:1, display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
                                <Typography
                                    variant='h1'
                                    textAlign='center'
                                    color='commmon.white'
                                    fontWeight={800}
                                >
                                    Testudog
                                </Typography>

                                <Typography
                                    variant='h5'
                                    textAlign='left'
                                    color='commmon.white'
                                    sx={{my:3, py:3}}
                                >
                                    Set forth with Testudog, Robotics at Maryland’s latest project and <span style={{color: theme.palette.ternary.main, fontWeight:800}}> University of Maryland’s first quadrupedal robot</span>. Testudog is an all terrain research and development platform. 
                                </Typography>
                                <Button size='large' variant='contained' color='ternary' href='/robots/testudog'>
                                    <Typography variant='h5' color='common.black' >Learn More</Typography>
                                </Button>
                            </Container>
                            
                        </Grid>
                        <Grid item xs={12} md={6} minHeight={{xs:600, md:800}} >
                            <Image src={TestudogShot.src} alt={TestudogShot.alt}/>
                        </Grid>
                        
                        

                    </Grid>

                </Container>
            </Box>

            {/* Call to Action */}
            <Container
                id='calltoaction'
                maxWidth='false'
                sx={{
                    backgroundImage: `url(${IdeaStepsImage.src})`,
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',
                    height:{xs:1200, lg:770},
                    display:'flex',
                    flexDirection:'column',
                    justifyContent:'space-evenly',
                    title:IdeaStepsImage.alt,
                    py:8
                }}

            >
                <Typography variant='h2' textAlign='center' color='commmon.white' fontWeight={800}>Get Involved</Typography>
                <Grid container textAlign='center' spacing={10} >

                    <Grid item xs={12} md={4} >
                        <Container maxWidth='xs' sx={{display:'flex', flexDirection:'column', alignItems:'center', justifyContent:'space-between', height:1, maxHeight:350}}>
                            <Typography variant='h4' color='commmon.white' fontWeight={600} sx={{py:3}}>For Sponsors</Typography>
                            <Typography paragraph color='commmon.white' textAlign='left' fontSize={18} sx={{py:3}}>Interested in supporting our work? Robotics at Maryland is actively looking for sponsors to help us fund and develop new projects, and to support us as we compete in the 2023 International Robosub competition.</Typography>
                            <Button size='large' variant='contained' href='/sponsors'> <Typography variant='h6'>Sponsor Us</Typography></Button>
                        </Container>
                        {/* <Grid item xs={12} sx={{py:3}}>
                            <Typography variant='h4' color='commmon.white'>For Sponsors</Typography>
                        </Grid>
                        <Grid item xs={12} sx={{py:3}}>
                            <Typography paragraph color='commmon.white' textAlign='left'>Interested in supporting our work? Robotics at Maryland is actively looking for sponsors to help us fund and develop new projects, and to support us as we compete in the 2023 International Robosub competition.</Typography>
                        </Grid>
                        <Grid item xs={12} >
                            <Button size='large' variant='contained' href='/sponsors'> <Typography variant='h6'>Sponsor Us</Typography></Button>
                        </Grid> */}
                        
                    </Grid>

                    <Grid item xs={12} md={4} >
                        <Container maxWidth='xs' sx={{display:'flex', flexDirection:'column', alignItems:'center', justifyContent:'space-between', height:1, maxHeight:350}}>
                            <Typography variant='h4' color='commmon.white' fontWeight={600} sx={{py:3}}>For New Members</Typography>
                            <Typography paragraph color='commmon.white' textAlign='left' fontSize={18} sx={{py:3}}> Want to build robots? We are open to students of all majors and skill levels, and welcome students with or without past technical experience </Typography>
                            <Button size='large' variant='contained'><Typography variant='h6' color='commmon.white'>Join Us</Typography></Button>
                        </Container>
                        {/* <Grid item xs={12} sx={{py:3}}>
                            <Typography variant='h4' color='commmon.white'>For New Members</Typography>
                        </Grid>
                        <Grid item xs={12} sx={{py:3}}>
                            <Typography paragraph color='commmon.white' textAlign='left'> Want to build robots? We are open to students of all majors and skill levels, and welcome students with or without past technical experience </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Button size='large' variant='contained'><Typography variant='h6' color='commmon.white'>Join Us</Typography></Button>
                        </Grid> */}
                        
                    </Grid>

                    <Grid  item xs={12} md={4} /*{spacing={4}}*/>
                        <Container maxWidth='xs' sx={{display:'flex', flexDirection:'column', alignItems:'center', justifyContent:'space-between', height:1, maxHeight:350}}>
                        {/* <Grid item xs={12} sx={{py:3}}> */}
                            <Typography variant='h4' sx={{m:3}} fontWeight={600} color='commmon.white'>Join Our Newsletter</Typography>
                        {/* </Grid> */}
                        {/* <Grid item xs={12} sx={{py:3}}> */}
                            <TextField label='Email Address' variant='filled' size='large' sx={{m:3}}/>

                            <Button size='large' variant='contained'><Typography variant='h6' color='commmon.white'>Subscribe</Typography></Button>
                        </Container>
                        {/* </Grid> */}
                    </Grid>
                </Grid>
            </Container>
        </>

    )

}
