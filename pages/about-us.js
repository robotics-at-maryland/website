'use client';

import { Container, Typography, Grid, Divider, Box, Button } from '@mui/material';
import * as React from 'react';
import Image from '@/components/Image';
import Metric from '@/components/Metric';
import Email from '@mui/icons-material/Email';
import SEO from "@/components/SEO";

const Team = {
    src: '/team-hero.jpg',
    alt: 'Robotics At Maryland Team'
}
const Electronics = {
    src: '/Electronics.png',
    alt: 'Picture of a power supply and a soldering iron'
}
const Mechanical = {
    src: '/CADModel.png',
    alt: 'A CAD assembly'
}
const Software = {
    src: '/Stereo.png',
    alt: 'A Stereo camera raw output'
}
export default function AboutUs() {
    return (
        <>
            <SEO/>
            <Box sx={{bgcolor:'common.white', color:'common.black'}}>
                <Container
                    maxWidth='xl'
                    sx={{
                        pt: 10
                    }}
                >
                    {/* <Breadcrumbs aria-label="breadcrumb">
                    <Link underline="hover" color="inherit" href="/">
                        Home
                    </Link>
                    <Typography color="text.primary"> About Us </Typography>
                    </Breadcrumbs> */}

                    {/* Hero Image */}

                    <Typography variant="h1" noWrap fontWeight={800} sx={{ m: 1 }}> Our Team </Typography>
                    <Image
                        src={Team.src}
                        alt={Team.alt}
                        sx={{
                            height: 800,
                            width: 1,
                            display: "block",
                            justifyContent: "center",
                            alignItems: "center",
                            textAlign: "center"
                        }}
                    />
                
                    
                
                    <Grid
                        container
                        justifyContent='center'
                        textAlign='center'
                        spacing={5}
                        sx={{p:5}}
                    >
                        {LeadershipData.map((item) => (
                            <Grid item xs='auto' key={item.name}>
                                <Image src={item.src} alt={item.name} width={200} height={200} />
                                <Typography variant='subtitle1'>{item.name}</Typography>
                                <Typography variant='subtitle2'>{item.position}</Typography>
                                <Typography variant='subtitle2'>{item.major + ' \'' + item.gradYear}</Typography>
                                <Button href={item.contact} startIcon={<Email/>} variant='outlined' size='small'>Contact</Button>
                            </Grid>
                        ))}
                    </Grid>
                    
                </Container>

                {/* Electrical */}
                <Divider />
                {/* <Typography variant="h2" textAlign='center'> Electrical Team</Typography> */}
                {/* Banner */}
                <Grid container textAlign='center' bgcolor='primary.main' color='common.white' spacing={10} sx={{mt:3, py:10}}>
                    
                    <Grid item xs={12}>
                        <Typography variant="h2" textAlign='center' fontWeight={800}> Electrical Team</Typography>
                    </Grid>
                    <Grid item xs={12} lg={6} minHeight={500}>
                        <Image src={Electronics.src} alt={Electronics.alt}/>
                    </Grid>
                       
                        
                    <Grid item xs={6}>
                        <Container maxWidth='sm' disableGutters  sx={{px:4, height:1, display:'flex', flexDirection:'column', justifyContent:'space-evenly'}}>
                            <Typography paragraph variant='h4' textAlign='left' fontWeight={600}> Soldering, building circuits, messing around with signals</Typography>
                            <Typography paragraph variant='h4' textAlign='left' fontWeight={600}> USB 3.0 termination experts</Typography>
                        </Container>
                    </Grid>
                        
                    
                    
                </Grid>
                {/* Member Images */}
                <Container maxWidth='xl' sx={{py: 8}}>
                    
                    <Grid
                        container
                        justifyContent='center'
                        textAlign='center'
                        spacing={5}
                    >
                        {ElectricalData.map((item) => (
                            <Grid item xs='auto' key={item.name} >
                                {/* <Image src={item.img} alt={item.name} width={200} height={200}/> */}
                                <Typography variant='subtitle1' color='common.black'>{item.name}</Typography>
                                <Typography variant='subtitle2' color='common.black'>{item.major}</Typography>

                            </Grid>
                        ))}
                    </Grid>
                </Container>
                
                {/* Mechanical */}
                <Divider />
                {/* Banner */}
                <Grid container textAlign='center' bgcolor='primary.main' color='common.white' spacing={10} sx={{mt:3, py:10}}>

                    <Grid item xs={12}>
                        <Typography variant="h2" textAlign='center' fontWeight={800}> Mechanical Team</Typography>
                    </Grid>
                    <Grid item xs={12} lg={6} minHeight={500}>
                        <Image src={Mechanical.src} alt={Mechanical.alt} />
                    </Grid>
                        
                    <Grid item xs={6}>
                        <Container maxWidth='sm' disableGutters  sx={{px:4, height:1, display:'flex', flexDirection:'column', justifyContent:'space-evenly'}}>
                            <Typography paragraph variant='h4' textAlign='left' fontWeight={600}> Prototyping, designing, and installing physical components</Typography>
                            <Typography paragraph variant='h4' textAlign='left' fontWeight={600}> Never grew out of LEGOs</Typography>
                        </Container>
                    </Grid>
                    

                    
                </Grid>
                <Container maxWidth='xl' sx={{py: 8}}>
                    
                    <Grid
                        container
                        justifyContent='center'
                        textAlign='center'
                        spacing={5}
                    >
                        {MechanicalData.map((item) => (
                            <Grid item xs='auto' key={item.name}>
                                {/* <Image src={item.img} alt={item.name} width={200} height={200} /> */}
                                <Typography variant='subtitle1'>{item.name}</Typography>
                                <Typography variant='subtitle2'>{item.major}</Typography>

                            </Grid>
                        ))}
                    </Grid>
                </Container>
                
                {/* Software */}
                <Divider />
                
                {/* Banner */}
                <Grid container textAlign='center' bgcolor='primary.main' color='common.white' spacing={10} sx={{mt:3, py:10}}>
                    <Grid item xs={12}>
                        <Typography variant="h2" textAlign='center' fontWeight={800}> Software Team </Typography>
                    </Grid>
                    <Grid item xs={12} lg={6} minHeight={500}>
                        <Image src={Software.src} alt={Software.alt}/>
                    </Grid>
                   
                        
                    <Grid item xs={6}>
                        <Container maxWidth='sm' disableGutters  sx={{px:4, height:1, display:'flex', flexDirection:'column', justifyContent:'space-evenly'}}>
                            <Typography paragraph variant='h4' textAlign='left' fontWeight={600}> Processing sensor data, programming computer vision, and controlling robots </Typography>
                            <Typography paragraph variant='h4' textAlign='left' fontWeight={600}> Pleasing our AI overlords </Typography>
                        </Container>
                    </Grid>
                       
                    
                    
                    
                </Grid>
                <Container maxWidth='xl' sx={{py: 10}}>
                    <Grid
                        container
                        justifyContent='center'
                        textAlign='center'
                        spacing={5}
                    >
                        {SoftwareData.map((item) => (
                            <Grid item xs='auto' key={item.name}>
                                {/* <Image src={item.img} alt={item.name} width={200} height={200}/> */}
                                <Typography variant='subtitle1'>{item.name}</Typography>
                                <Typography variant='subtitle2'>{item.major}</Typography>

                            </Grid>
                        ))}
                    </Grid>
                </Container>

            </Box>
        </>
    )
}

const LeadershipData = [
    
    {
        src: '/Joshua_Smith.png',
        name: 'Joshua Smith',
        position: 'President',
        major: 'Computer Science & Math',
        gradYear: 23,
        contact:'mailto:team@ram.umd.edu'
    },
    {
        src: '/Dillon_Capalongo.jpg',
        name: 'Dillon Capalongo',
        position: 'Vice President, Mechanical Lead',
        major: 'Mechanical Engineering',
        gradYear: 24,
        contact:'mailto:capadill@umd.edu'
    },
    {
        src: '/Andrew_Weller.jpg',
        name: 'Andrew Weller',
        position: 'Electrical Lead',
        major: 'Electrical Engineering',
        gradYear: 24,
        contact: 'mailto:aweller1@umd.edu'
    },
    {
        src: '/Jeffrey_Fisher.jpg',
        name: 'Jeffrey Fisher',
        position: 'Software Lead',
        major: 'Computer Science',
        gradYear: 25,
        contact: 'mailto:jeffrey@jeffreyfisher.net'
    }
];

const MechanicalData = [
    { name: 'Dillon Capalongo', major: 'Mechanical Engineering' },
    { name: 'Joshua Ehizibolo', major: 'Mechanical Engineering' },
    { name: 'Dominik Blaho', major: 'Aerospace Engineering' },
    { name: 'Zachary Friedman-hill', major: 'Mechanical Engineering' },
    { name: 'Maanav Subramanian', major: 'Aerospace Engineering' },
    { name: 'Pranav Srinivasan', major: 'Mechanical Engineering' },
    { name: 'Sean Millman', major: 'Computer Science' },
    { name: 'Zoe Barbour', major: 'Environmental Engineering' },
    { name: 'Pranav Narayan', major: 'Aerospace Engineering' },
    { name: 'Akash Iyer', major: 'Mechanical Engineering' },
    { name: 'Evan Chen', major: 'Mechanical Engineering' },
    { name: 'Manny Gancayco', major: 'Mechanical Engineering' },
    { name: 'Calvin Dunn', major: 'Mechanical Engineering' },
    { name: 'Ilan Davidovsky', major: 'Mechanical Engineering' },
    { name: 'Ankit Verghese', major: 'Aerospace Engineering' },
    { name: 'Rose McKentry', major: 'Mechanical Engineering' },
    { name: 'Gianni Sperduto', major: 'Mechanical Engineering' },
    { name: 'Carolyn Lee', major: 'Chemical Engineering' },
    { name: 'Khoa Nguyen', major: 'Mechanical Engineering' },
    { name: 'Eli Mirny', major: 'Aerospace Engineering' },
    { name: 'Shaunak Roy', major: 'Mechanical Engineering' },
    { name: 'Rachel Berley', major: 'Mechanical Engineering' },
    { name: 'Asha Gaines', major: 'Mechanical Engineering' },
    { name: 'Thomas Wolcott', major: 'Computer Science' },
    { name: 'Russel Chomnou', major: 'Mechanical Engineering' },
    { name: 'Josh Rippeon', major: 'Mechanical Engineering' },
    { name: 'Alexander Teacu', major: 'Mechanical Engineering' },
    { name: 'Justin Cheng', major: "Professional Master's for Robotics" },
    { name: 'Nicolas Lei', major: 'Mechanical Engineering' },
    { name: 'Rae Defrancesco', major: 'Mechanical Engineering' },
    { name: 'Logan Brooks', major: 'Mechanical Engineering' }

];

const ElectricalData = [
    { name: 'Aditya Shelke', major: 'Computer Science & Math' },
    { name: 'Brian Zagalsky', major: 'Electrical Engineering' },
    { name: 'Lawrence Rhoads', major: 'Computer Engineering' },
    { name: 'Daniel McLawhorn', major: 'Electrical Engineering' },
    { name: 'Gavin Bogdan', major: 'Electrical Engineering' },
    { name: 'Erik Chapman', major: 'Electrical Engineering' },
    { name: 'Andrew Schaefer', major: 'Electrical Engineering' },
    { name: 'CC Colangelo', major: 'Electrical Engineering' },
    { name: 'Jeremy Yun', major: 'Electrical Engineering' },
    { name: 'David Nahorniac', major: 'Electrical Engineering' },
    { name: 'Drew Weller', major: 'Electrical Engineering' }
];

const SoftwareData = [
    { name: 'Alexander Cruz', major: 'Computer Science' },
    { name: 'Clara Gong', major: 'Computer Science' },
    { name: 'Alexander Yelovich', major: 'Computer Science and Math' },
    { name: 'Thomas Li', major: 'Computer Science' },
    { name: 'Ishaan Ghosh', major: 'Computer Science' },
    { name: 'Gautham Hari', major: 'Computer Science' },
    { name: 'Srivishnu Piratla', major: 'Computer Science' },
    { name: 'Dashawna Lara', major: 'Computer Science' },
    { name: 'Jeffrey Fisher', major: 'Computer Science' },
    { name: 'Josh Smith', major: 'Computer Science & Math' },
    { name: 'Shashank Singh', major: 'Computer Science' },
    { name: 'Adam Malyshev', major: 'Computer Science' }
];