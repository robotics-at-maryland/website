'use client';
import Layout from "@/components/Layout";
import { ThemeProvider, createTheme, responsiveFontSizes } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import {red} from '@mui/material/colors';
import '../styles/global.css';
/* Font Imports */
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import '@fontsource/oswald/500.css'; 


let theme = createTheme({
    palette: {
        mode:'dark',
        primary: {
            main: "#E21833",
            alternate: red[500]    //red
        },
        secondary: {
            main: "#F9F2E6"     // This is a cream color that is a shade of white. 
        },
        ternary: {
            main: "#ffd200"     // gold
        }
    },

    typography: {
        fontFamily: [
          'Roboto',
          'Oswald'
        ].join(','),
      }
  });

theme = responsiveFontSizes(theme);



export default function App({Component, pageProps}){
    return(
        <ThemeProvider theme={theme}>
            <CssBaseline/>
            <Layout>
                <Component {...pageProps}/>
            </Layout>
        </ThemeProvider>
        
    )
}